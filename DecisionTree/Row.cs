﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionTree
{
    public class Row
    {
        public Row(int fieldsNumber)
        {
            this.fields = new bool[fieldsNumber];
        }
        public Row(bool[] fields)
        {
            this.fields = fields;
        }

        private bool[] fields;

        public bool this[int index]
        {
            get { return this.fields[index]; }
            set { this.fields[index] = value; }
        }
    }
}
