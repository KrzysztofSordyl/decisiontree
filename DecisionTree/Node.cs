﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionTree
{
    public class Node
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">Tabela z danymi</param>
        public Node(Data data): this(data, new List<string>())
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">Tabela z danymi</param>
        /// <param name="previousDivisions">Poprzednio zadane pytania</param>
        public Node(Data data, List<string> previousDivisions)
        {
            this.Data = data;
            this.previousDivisions = previousDivisions;
            this.resolve();
        }

        private List<string> previousDivisions;
        public Data Data { get; private set; }

        /// <summary>
        /// Zawiera kolumnę z zadawanym pytaniem lub z konkluzją
        /// </summary>
        public string Column { get; private set; }
        /// <summary>
        /// Obliczony information gain dla kolumn (w wypadku gdy nie doszliśmy do konkluzji)
        /// </summary>
        public Dictionary<string, double> InformationGain { get; private set; }

        public Node True { get; private set; }
        public Node False { get; private set; }

        private void resolve()
        {
            // sprawdzanie czy mamy jedną konkluzję
            bool isEnd = false;
            foreach (string option in this.Data.Options)
            {//esi2017 tsis2017
                int index = this.Data.IndexOf(option);
                if (this.Data.Rows.All(r => r[index]))
                {
                    isEnd = true;
                    this.Column = option;
                    break;
                }
            }
            if (!isEnd)
            {
                // wyznaczanie następnego pytania
                double entropy = this.calculateEntropy();
                this.InformationGain = this.Data.Columns.Where(c => !this.previousDivisions.Contains(c) && this.Data.Rows.Any(r => r[this.Data.IndexOf(c)]) && this.Data.Rows.Any(r => !r[this.Data.IndexOf(c)])).ToDictionary(c => c, c => entropy - this.calculateEntropy(c));
                this.Column = this.InformationGain.First(x => x.Value == this.InformationGain.Max(y => y.Value)).Key;
                // podział danych na dwa podzbiory
                var splitedData = this.Data.Split(this.Data.IndexOf(this.Column));
                List<string> previousColumns = new List<string>();
                previousColumns.Add(this.Column);
                previousColumns.AddRange(this.previousDivisions);
                this.True = new Node(splitedData.Key, previousColumns);
                this.False = new Node(splitedData.Value, previousColumns);
            }
        }

        private double calculateEntropy()
        {
            return this.calculateEntropy(this.Data);
        }
        private double calculateEntropy(Data data)
        {
            return -data.Options.Select(o => data.IndexOf(o)).Sum(i =>
            {
                double p = (double)data.Rows.Count(r => r[i]) / data.Rows.Count;
                if (p == 0) return 0;
                else return p * Math.Log(p, 2);
            });
        }

        private double calculateEntropy(string column)
        {
            return this.calculateEntropy(column, this.Data);
        }
        private double calculateEntropy(string column, Data data)
        {
            int index = this.Data.IndexOf(column);
            var splitedData = this.Data.Split(index);
            return (splitedData.Key.Rows.Count * this.calculateEntropy(splitedData.Key) + splitedData.Value.Rows.Count * this.calculateEntropy(splitedData.Value)) / this.Data.Rows.Count;
        }
    }
}
