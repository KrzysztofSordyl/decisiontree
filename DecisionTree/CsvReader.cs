﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionTree
{
    public class CsvReader
    {
        public CsvReader(string filename)
        {
            this.filename = filename;
        }

        private string filename;

        public Data Read()
        {
            IEnumerable<string[]> lines = File.ReadLines(this.filename).Select(l => l.Split(';'));

            var headers = lines.Take(2);
            // 1. wiersz: 0 gdy kolumna jest przesłanką, 1 gdy kolumna jest konkluzją
            bool[] isOption = headers.ElementAt(0).Select(f => f != "0").ToArray();
            // 2. wiersz to nazwy kolumn
            string[] header = headers.ElementAt(1);
            // następne wiersze to dane
            List<string> columns = new List<string>();
            List<string> options = new List<string>();
            for (int i = 0; i < header.Length; i++)
            {
                if (isOption[i]) options.Add(header[i]);
                else columns.Add(header[i]);
            }

            List<Row> rows = new List<Row>();
            foreach (bool[] fields in lines.Skip(2).Select(l => l.Select(f => f != "0").ToArray()))
            {
                Array.Sort(isOption.Select(x => x ? 1 : 0).ToArray(), fields);
                rows.Add(new Row(fields));
            }

            return new Data(columns.ToArray(), options.ToArray())
            {
                Rows = rows
            };
        }
    }
}
