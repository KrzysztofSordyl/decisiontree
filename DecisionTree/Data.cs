﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionTree
{
    public class Data
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="columns">przesłanki</param>
        /// <param name="options">konkluzje</param>
        public Data(string[] columns, string[] options)
        {
            this.Columns = columns;
            this.Options = options;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="structure">Dane, z których kopiowana jest struktura</param>
        public Data(Data structure) : this(structure.Columns, structure.Options)
        {

        }

        public string[] Columns { get; private set; }
        public string[] Options { get; private set; }

        public List<Row> Rows { get; set; } = new List<Row>();

        public int IndexOf(string column)
        {
            if (this.Columns.Contains(column))
                return Array.IndexOf(this.Columns, column);
            else
                return this.Columns.Length + Array.IndexOf(this.Options, column);
        }

        public KeyValuePair<Data, Data> Split(int column)
        {
            Data dataTrue = new Data(this);
            Data dataFalse = new Data(this);

            foreach(Row row in this.Rows)
            {
                if (row[column])
                    dataTrue.Rows.Add(row);
                else
                    dataFalse.Rows.Add(row);
            }

            return new KeyValuePair<Data, Data>(dataTrue, dataFalse);
        }
    }
}
