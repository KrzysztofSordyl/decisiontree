﻿using DecisionTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //Node tree = new Node(
            //    new Data(
            //        new string[] { "wiek: <20", "wiek: 20-30", "wiek: >30", "płeć: K", "płeć: M", "mieszka: wieś", "mieszka: miasto" },
            //        new string[] { "reklama: Internet", "reklama: Prasa", "reklama: Telewizja" })
            //    {
            //        Rows = new List<Row>
            //        {
            //            new Row(new bool[] { true, false, false, true, false, false, true, true, false, false }),
            //            new Row(new bool[] { true, false, false, false, true, false, true, true, false, false }),
            //            new Row(new bool[] { false, true, false, false, true, false, true, false, true, false }),
            //            new Row(new bool[] { false, false, true, false, true, false, true, false, true, false }),
            //            new Row(new bool[] { false, true, false, true, false, false, true, false, true, false }),
            //            new Row(new bool[] { true, false, false, false, true, true, false, false, false, true }),
            //            new Row(new bool[] { true, false, false, true, false, true, false, false, false, true }),
            //            new Row(new bool[] { false, true, false, true, false, true, false, false, false, true }),
            //            new Row(new bool[] { false, true, false, false, true, true, false, false, false, true }),
            //            new Row(new bool[] { false, false, true, true, false, true, false, false, false, true }),
            //            new Row(new bool[] { false, false, true, true, false, false, true, false, false, true }),
            //            new Row(new bool[] { false, false, true, false, true, true, false, false, false, true }),
            //        }
            //    },
            //    new List<string>());

            Node tree = new Node(new CsvReader("data.csv").Read());
            writeNode(tree);
            Console.ReadLine();
        }

        private static int tabs = 0;
        private static void writeNode(Node node)
        {
            if (node.True != null)
            {
                int maxLength = Math.Max(node.InformationGain.Keys.Max(x => x.Length), node.InformationGain.Values.Max(x => x.ToString().Length));
                Console.WriteLine(string.Join(" ", node.InformationGain.Keys.Select(x => x.ToString().PadRight(maxLength))));
                Console.WriteLine(string.Join(" ", node.InformationGain.Values.Select(x => x.ToString().PadRight(maxLength))));
            }
            Console.WriteLine(new string('\t', tabs) + node.Column);
            if (node.True != null)
            {
                tabs++;
                Console.WriteLine(new string('\t', tabs) + "True");
                writeNode(node.True);
                Console.WriteLine(new string('\t', tabs) + "False");
                writeNode(node.False);
                tabs--;
            }
        }
    }
}
